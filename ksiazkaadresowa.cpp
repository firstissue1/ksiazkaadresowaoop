#include "KsiazkaAdresowa.h"

void KsiazkaAdresowa::wyloguj()
{
    uzytkownikManager.wylogujUzytkownika();
    delete adresatManager;
    adresatManager = NULL;
}

void KsiazkaAdresowa::rejestracjaUzytkownika()
{
    uzytkownikManager.rejestracjaUzytkownika();
}

void KsiazkaAdresowa::wypiszWszystkichUzytkownikow()
{
    uzytkownikManager.wypiszWszystkichUzytkownikow();
}

void KsiazkaAdresowa::zaloguj()
{
    uzytkownikManager.logowanieUzytkownika();
    if (uzytkownikManager.czyUzytkownikSieZalogowal())
    {
        adresatManager = new AdresatManager(NAZWA_PLIKU, uzytkownikManager.pobierzIdZalogowanegoUzytkownika());
    }
}

void KsiazkaAdresowa::zmienHaslo()
{
    uzytkownikManager.zmianaHaslaZalogowanegoUzytkownika();
}

void KsiazkaAdresowa::wypiszWszystkichAdresatow()
{
    adresatManager->wyswietlWszystkichAdresatowUzytkownika();
}

void KsiazkaAdresowa::dodajAdresata()
{
    if (uzytkownikManager.czyUzytkownikSieZalogowal())
    {
        adresatManager->dodajAdresata();
    }
    else
    {
        cout << "Aby dodac adresata, nalezy sie zalogowac." << endl;
        system("pause");
    }
}

void KsiazkaAdresowa::usunAdresata()
{
    adresatManager->usunAdresata();
}

void KsiazkaAdresowa::edytujAdresata()
{
    adresatManager->edytujAdresata();
}

void KsiazkaAdresowa::wyszukajAdresatowPoImieniu()
{
    adresatManager->wyszukajAdresatowPoImieniu();
}

void KsiazkaAdresowa::wyszukajAdresatowPoNazwisku()
{
    adresatManager->wyszukajAdresatowPoNazwisku();
}

bool KsiazkaAdresowa::czyUzytkownikJestZalogowany()
{
    return uzytkownikManager.czyUzytkownikSieZalogowal();
}
