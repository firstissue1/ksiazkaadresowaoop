#include <iostream>

#include "KsiazkaAdresowa.h"
#include "MetodyPomocnicze.h"

using namespace std;

int _main()
{
    PlikZAdresatami plikZAdresatami("Adresaci-test.txt");
    Adresat adresat(1,1,"J","J","9","@","ul.");

    plikZAdresatami.dopiszAdresataDoPliku(adresat);

    adresat.ustawImie("Marek");
    plikZAdresatami.dopiszAdresataDoPliku(adresat);

//    plikZAdresatami.usunWybranegoAdresataZPliku(1);
}


int main()
{
KsiazkaAdresowa ksiazkaAdresowa("Uzytkownicy.txt", "Adresaci.txt");
char wybor;

while (true)
{
    if (ksiazkaAdresowa.czyUzytkownikJestZalogowany() == 0)
    {
        wybor = MetodyPomocnicze::wybierzOpcjeZMenuGlownego();

        switch (wybor)
        {
        case '1': ksiazkaAdresowa.rejestracjaUzytkownika(); break;
        case '2': ksiazkaAdresowa.zaloguj(); break;
        case '9': exit(0); break;
        default:
            cout << endl << "Nie ma takiej opcji w menu." << endl << endl;
            system("pause");
            break;
        }
    }
    else
    {
        wybor = MetodyPomocnicze::wybierzOpcjeZMenuUzytkownika();

        switch (wybor)
        {
        case '1': ksiazkaAdresowa.dodajAdresata();
            break;
        case '2': ksiazkaAdresowa.wyszukajAdresatowPoImieniu();
            break;
        case '3': ksiazkaAdresowa.wyszukajAdresatowPoNazwisku();
            break;
        case '4': ksiazkaAdresowa.wypiszWszystkichAdresatow();
            break;
        case '5': ksiazkaAdresowa.usunAdresata();
            break;
        case '6': ksiazkaAdresowa.edytujAdresata();
            break;
        case '7': ksiazkaAdresowa.zmienHaslo(); break;
        case '8': ksiazkaAdresowa.wyloguj(); break;
        }
    }
}

return 0;
}
