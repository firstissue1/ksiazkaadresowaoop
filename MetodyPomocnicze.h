#ifndef METODYPOMOCNICZE_H
#define METODYPOMOCNICZE_H

#include <iostream>
#include <algorithm>
#include <sstream>

#include "Adresat.h"

using namespace std;

class MetodyPomocnicze
{
    static int wczytajLiczbeCalkowita();

public:
    static char wybierzOpcjeZMenuGlownego();
    static char wybierzOpcjeZMenuUzytkownika();
    static char wybierzOpcjeZMenuEdycja();
    static char wczytajZnak();
    static void wyswietlIloscWyszukanychAdresatow(int iloscAdresatow);
    static int podajIdWybranegoAdresata();
    static Adresat pobierzDaneAdresata(string daneAdresataOddzielonePionowymiKreskami);
    static int pobierzIdUzytkownikaZDanychOddzielonychPionowymiKreskami(string daneJednegoAdresataOddzielonePionowymiKreskami);
    static int pobierzIdAdresataZDanychOddzielonychPionowymiKreskami(string daneJednegoAdresataOddzielonePionowymiKreskami);
    static string zamienDaneAdresataNaLinieZDanymiOddzielonymiPionowymiKreskami(Adresat adresat);
    static string zamienPierwszaLitereNaDuzaAPozostaleNaMale(string tekst);
    static string konwerjsaIntNaString(int liczba);
    static string wczytajLinie();
    static string pobierzLiczbe(string tekst, int pozycjaZnaku);
    static int konwersjaStringNaInt(string liczba);

};

#endif
