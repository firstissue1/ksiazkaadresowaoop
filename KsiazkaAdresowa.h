#ifndef KSIAZKAADRESOWA_H
#define KSIAZKAADRESOWA_H

#include <iostream>

#include "UzytkownikManager.h"
#include "AdresatManager.h"

using namespace std;

class KsiazkaAdresowa
{
    UzytkownikManager uzytkownikManager;
    AdresatManager *adresatManager;

    const string NAZWA_PLIKU;

public:
    KsiazkaAdresowa(string nazwaPlikuZUzytkownikami, string nazwaPlikuZAdresatami) : uzytkownikManager(nazwaPlikuZUzytkownikami), NAZWA_PLIKU(nazwaPlikuZAdresatami) {
        adresatManager = NULL;
    };
    ~KsiazkaAdresowa(){
        delete adresatManager;
        adresatManager = NULL;
    };
    void rejestracjaUzytkownika();
    void wypiszWszystkichUzytkownikow();
    void zaloguj();
    void wyloguj();
    void zmienHaslo();
    void wypiszWszystkichAdresatow();
    void dodajAdresata();
    void usunAdresata();
    void edytujAdresata();
    void wyszukajAdresatowPoImieniu();
    void wyszukajAdresatowPoNazwisku();
    bool czyUzytkownikJestZalogowany();
};

#endif
