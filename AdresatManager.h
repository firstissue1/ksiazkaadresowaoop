#ifndef ADRESATMANAGER_H
#define ADRESATMANAGER_H

#include <fstream>
#include <vector>

#include "Adresat.h"
#include "PlikZAdresatami.h"

class AdresatManager
{
    PlikZAdresatami plikZAdresatami;
    const int ID_ZALOGOWANEGO_UZYTKOWNIKA;
    vector <Adresat> adresaci;

    Adresat podajDaneNowegoAdresata();
    void wyswietlDaneAdresata(Adresat adresat);
    void zaktualizujDaneWybranegoAdresata(Adresat adresat);

public:
    AdresatManager(string nazwaPlikuZAdresatami, int idZalogowanegoUzytkownika)
     : plikZAdresatami(nazwaPlikuZAdresatami),
     ID_ZALOGOWANEGO_UZYTKOWNIKA(idZalogowanegoUzytkownika)
    {
        adresaci = plikZAdresatami.wczytajAdresatowZalogowanegoUzytkownikaZPliku(ID_ZALOGOWANEGO_UZYTKOWNIKA);
    };
    void wyszukajAdresatowPoNazwisku();
    void wyszukajAdresatowPoImieniu();
    void edytujAdresata();
    void dodajAdresata();
    void usunAdresata();
    void wyswietlWszystkichAdresatowUzytkownika();
};

#endif
